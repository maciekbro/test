## Testing BitBucket functionality

## Installation

The code is build with Gradle. Wrapper properties file to start a build with specific Gradle version is provided.

## Usage

## Tests

Tests are run with Spock test framework

## License

The code is free to use and modify.